; (in-package #:doors)

;; (require :swank)
;; (swank:create-server :port 4004
;;                      :style swank:*communication-style*
;;                      :dont-close t)

;; Try to start up a slynk server
(push #p"~/common-lisp/sly/" asdf:*central-registry*)
(asdf:load-system :slynk)
(slynk:create-server :port 4004)
