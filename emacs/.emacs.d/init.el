;; init.el --- Personal GNU Emacs configuration file.

;;; Code:

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(require 'garbage-collect)

(require 'package)

;; (setq gc-cons-threshold-original gc-cons-threshold)
;; (setq gc-cons-threshold (* 1024 1024 100))

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Initialise the packages, avoiding a re-initialisation.
(unless (bound-and-true-p package--initialized)
 (setq package-enable-at-startup nil)
 (package-initialize))

;; Make sure `use-package' is available.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Configure `use-package' prior to loading it.
(eval-and-compile
  (setq use-package-always-ensure nil)
  (setq use-package-always-defer nil)
  (setq use-package-always-demand nil)
  (setq use-package-expand-minimally nil)
  (setq use-package-enable-imenu-support t))

(menu-bar-mode 1)
(tool-bar-mode -1)

(eval-when-compile
  (require 'use-package))

;; Set the default 
(set-frame-font "Fixedsys Excelsior")

;; (defconst fixedsys-excelsior-font-lock-keywords-alist
;;   (mapcar (lambda (regex-char-pair)
;; 	    `(,(car regex-char-pair)
;; 	      (0 (prog1 ()
;; 		   (compose-region (match-beginning 1)
;; 				   (match-end 1)
;; 				   ;; The first argument to concat is a string containing a literal tab
;; 				   ,(concat "	" (list (decode-char 'ucs (cadr regex-char-pair)))))))))t
;; 	  '(("\\(>>=\\)"        #Xefb0f)
;; 	    ("\\(=<<\\)"        #Xefb1)
;; 	    ("\\(<\\*>\\)"      #Xefb2)
;; 	    ("\\(<\\$>\\)"      #Xefb3)
;; 	    ("\\(::\\)"         #Xefb4)
;; 	    ("\\(:=\\)"         #Xefb5)
;; 	    ("\\(<<<\\)"        #Xefb6)
;; 	    ("\\(>>>\\)"        #Xefb7)
;; 	    ("\\(<>\\)"         #Xefb8)
;; 	    ("\\(/=\\)"         #Xefb9)
;; 	    ("\\({-\\)"         #Xefba)
;; 	    ("\\(-}\\)"         #Xefbb)
;; 	    ("\\(<|\\)"         #Xefbc)
;; 	    ("\\(|>\\)"         #Xefbd)
;; 	    ("\\(~>\\)"         #Xefbe)
;; 	    ("\\(<~\\)"         #Xefbf)
;; 	    ("\\(<~>\\)"        #Xefc0)
;; 	    ("\\(<^>\\)"        #Xefc1)
;; 	    ("\\(/\\\\\\)"      #Xefc2)
;; 	    ("\\(<|>\\)"        #Xefc3)
;; 	    ("\\(>=>\\)"        #Xefc4)
;; 	    ("\\(<=<\\)"        #Xefc5))))


;; (defun add-fixedsys-excelsior-symbol-keywords ()
;;   (font-lock-add-keywords nil fixedsys-excelsior-font-lock-keywords-alist))

;; (add-hook 'prog-mode-hook
;;           #'add-fixedsys-excelsior-symbol-keywords)

(require 'org)
(setq vc-follow-symlinks t)

;;(org-babel-load-file (expand-file-name "~/.emacs.d/emacs-init.org"))
(org-babel-load-file (expand-file-name "~/.emacs.d/watkinsr-init.org"))

;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline success warning error])
 '(ansi-color-names-vector
   ["#000000" "#ff8059" "#44bc44" "#eecc00" "#33beff" "#feacd0" "#00d3d0" "#ffffff"])
 '(custom-safe-themes
   '("bdb095e04a145c15d48d8a4336a4d992267cb9c4a36da120d9ab1185771c8fc3" default))
 '(hl-todo-keyword-faces
   '(("HOLD" . "#e5f040")
     ("TODO" . "#feacd0")
     ("NEXT" . "#b6a0ff")
     ("THEM" . "#ed92f8")
     ("PROG" . "#00d3d0")
     ("OKAY" . "#4ae8fc")
     ("DONT" . "#58dd13")
     ("FAIL" . "#ff8059")
     ("DONE" . "#44bc44")
     ("NOTE" . "#f0ce43")
     ("KLUDGE" . "#eecc00")
     ("HACK" . "#eecc00")
     ("TEMP" . "#ffcccc")
     ("FIXME" . "#ff9977")
     ("XXX+" . "#f4923b")
     ("REVIEW" . "#6ae4b9")
     ("DEPRECATED" . "#aaeeee")))
 '(ibuffer-deletion-face 'dired-flagged)
 '(ibuffer-filter-group-name-face 'dired-mark)
 '(ibuffer-marked-face 'dired-marked)
 '(ibuffer-title-face 'dired-header)
 '(package-selected-packages
   '(mu4e modus-vivendi-theme yaml-mode which-key vterm visual-regexp use-package slime shr-tag-pre-highlight scala-mode sbt-mode rg rainbow-mode rainbow-blocks posframe peep-dired pass org-tree-slide org-bullets moody magit lsp-ui lsp-haskell keycast json-mode irony-eldoc htmlize goto-last-change git-timemachine ggtags flycheck-package flycheck-indicator exwm expand-region elfeed diredfl dired-subtree dired-rsync dired-narrow dired-git-info delight darkroom dap-mode company-lsp company-irony cider bongo beacon))
 '(safe-local-variable-values
   '((Package . MCCLIM-TRUETYPE)
     (Syntax . Common-Lisp)
     (indent-tabs)
     (Package . CLIM-INTERNALS)))
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   '((20 . "#ff8059")
     (40 . "#feacd0")
     (60 . "#ed92f8")
     (80 . "#f4923b")
     (100 . "#eecc00")
     (120 . "#e5f040")
     (140 . "#f8dec0")
     (160 . "#bfebe0")
     (180 . "#44bc44")
     (200 . "#58dd13")
     (220 . "#6ae4b9")
     (240 . "#4ae8fc")
     (260 . "#00d3d0")
     (280 . "#c6eaff")
     (300 . "#33beff")
     (320 . "#72a4ff")
     (340 . "#00baf4")
     (360 . "#b6a0ff")))
 '(vc-annotate-very-old-color nil)
 '(xterm-color-names
   ["#000000" "#ff8059" "#44bc44" "#eecc00" "#33beff" "#feacd0" "#00d3d0" "#a8a8a8"])
 '(xterm-color-names-bright
   ["#181a20" "#f4923b" "#58dd13" "#e5f040" "#72a4ff" "#ed92f8" "#4ae8fc" "#ffffff"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "POOP" :family "Fixedsys Excelsior")))))
