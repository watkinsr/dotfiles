(defvar my/enable-forgiving-gc t "Toggle forgiving gc logic.")
;; set a very large gc-cons-threshold (1GB) so we don't garbage
;; collect when I'm interacting with emacs ... BUT we also want
;; to prevent filling up to a large garbage pile when idle for
;; a long period of time (e.g. overnight), so we reschedule for
;; an interval seconds of idle time ... this way we should prevent
;; both gc when typing but also prevent gc-cons-threshold from
;; actually being reached and potentially triggering a very long gc
;; note: this handy if you want to run fancy modelines and other
;; things that aren't necessarily super memory efficient but either
;; way it generally makes emacs feel more snappy and responsive
(when my/enable-forgiving-gc
  (setq gc-cons-threshold (* 1024 1024 1024))
  ;; when we go idle for long periods of time, this is the interval at which gc occurs
  (defvar my/idle-timer-gc-idle-interval 60 "GC timer idle interval in seconds.")
  ;; how long we need to be idle for, for a single gc to occur ... keep this small
  (defvar my/idle-timer-gc-init-interval 2 "GC timer init interval in seconds.")
  (defvar my/idle-timer-gc-last nil "Last idle-timer-gc.")
  (defun my/idle-timer-gc ()
    "Force a garbage collect every MY/IDLE-TIMER-GC_INTERVAL consecutive seconds we are idle."
    ;; cancel any previous timers to prevent stale/unused timer stacking when user interrupts
    (when my/idle-timer-gc-last
      (cancel-timer my/idle-timer-gc-last))
    ;; don't repeat these, just have them stack on while we're idle
    (setq my/idle-timer-gc-last
          (run-with-idle-timer
           (time-add (current-idle-time) my/idle-timer-gc-idle-interval)
           nil 'my/idle-timer-gc))
    (garbage-collect))
  ;; kick off the initial timer, on a repeat so we kick in every time we go idle
  (run-with-idle-timer my/idle-timer-gc-init-interval t 'my/idle-timer-gc))

;; for debugging this malarky ... seems to work as intended
(setq garbage-collection-messages nil)

(provide 'garbage-collect)
