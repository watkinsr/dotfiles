export EDITOR=kak
export FILE=vifm

HOST=$(uname -n)

if [ $HOST = "x230" ];
then
    echo "LOG: x230 found"

    export BROWSER=firefox
	export TERMINAL=xterm
	export PAGER=less
	export LANG=en_US.UTF-8
	
	export GOPATH=$HOME/go

    export PATH=$PATH:$HOME/go/bin:$HOME/scripts/status:$HOME/scripts:$HOME/.local/bin

    # set ENV to a file invoked each time sh is started for interactive use.
    ENV=$HOME/.kshrc; export ENV

    export KAKOUNE_POSIX_SHELL=/usr/local/bin/dash
	export XDG_SESSION_TYPE=x11

    # Query terminal size; useful for serial lines.
    # if [ -x /usr/bin/resizewin ] ; then /usr/bin/resizewin -z ; fi

    # Display a random cookie on each login.
    if [ -x /usr/bin/fortune ] ; then /usr/bin/fortune freebsd-tips ; fi
    export HOME TERM
elif [ $HOST = "acer" ]; then
    echo "LOG: acer found"
	export XDG_SESSION_TYPE=x11

	export GOPATH=$HOME/go

    export BROWSER=firefox-bin
    export TERMINAL=xterm
    export PATH=$PATH:$HOME/scripts/status:$HOME/scripts
    export ENV="$HOME/.kshrc"
    export KAKOUNE_POSIX_SHELL=/bin/dash
else
	echo "Failed to find host: $HOST in environment"
fi
